{% set lsconf = salt['environ.get']('lsconf') %}
{% if lsconf %}


/etc/logstash/conf.d/{{ salt['environ.get']('lsconf') }}:
  file.managed:
    - log_file: /var/log/lspush.log
    - log_level_logfile: debug
    - source: /kmackay/gitted/elk-stack/logstash/conf.d/{{ salt['environ.get']('lsconf') }}
#   - template: jinja
    - user: logstash
    - group: logstash
    - mode: 0644

{% else %}

Fail - no environment passed in:
  test.fail_without_changes

{% endif %}
