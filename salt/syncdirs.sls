fetch_elk_repo:
  git.latest:
    - name: https://kmackay@bitbucket.org/kmackay/elk-stack.git
    - target: /home/kmackay/git_test
    - user: kmackay

/etc/logstash/pipelines.yml:
  file.copy:
    - source: /home/kmackay/git_test/logstash/pipelines.yml
    - force: True
    - user: logstash
    - group: logstash


/etc/logstash/:
  rsync.synchronized:
    - source: /home/kmackay/git_test/logstash/pipelines
    - delete: True


Fix_perms:
  file.directory:
    - name: /etc/logstash/pipelines
    - user: logstash
    - group: logstash
    - recurse:
      - user
      - group
