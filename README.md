# ELK Stack config files

This repo comprises three separate components

### Filebeat:

A client (filebeat) is installed on each host you're scraping the logs from. Filebeat is configured through a yaml file which defines the logs to be scanned and other processing options before it's sent to logstash

### Logstash:

Logstash is installed on dres. It's purpose is to receive incoming logs (being sent by filebeat), extract and modify the log lines according to how we want to see them in Kibana and then output to elasticsearch

### Elasticsearch/Kibana
