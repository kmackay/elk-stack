#For lines with non-null paths
%{BASE10NUM:stuff} \| %{IP:source_ip} \| %{URIPATHPARAM:path} \| webmasterId=102352&exchange=NYE&marketSession=NORMAL&stat=PG&includeTradeValueAndNumberOfTrades=true | %{DATESTAMP_OTHER} \| %{BASE10NUM:conns}

%{BASE10NUM:stuff} \| %{IP:source_ip} \| %{URIPATHPARAM:path} \| webmasterId=%{NUMBER:webmasterId}&exchange=%{WORD:exchange}&marketSession=%{WORD:marketsession}&stat=%{WORD:stat}&includeTradeValueAndNumberOfTrades=%{WORD:tradevalueandnumberoftrades} \| %{DATESTAMP_OTHER}


#Matches 98% of lines
^%{NUMBER:epoch} \| %{IP:source_ip} \| %{URIPATHPARAM:path} \| %{GREEDYDATA:the_rest} \| %{DATESTAMP_OTHER:date} \| %{NUMBER:connections}$



exchange=LSE&
marketSession=NORMAL&
stat=PG&
includeTradeValueAndNumberOfTrades=true
