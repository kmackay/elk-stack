#### Filebeat
  - Set document_type to app or syslog name. For example:
newscache for newscache app

#### Logstash
  - Put together at least one pattern match for a given log from a new app or server
and create numbered conf file in /etc/logstash/conf.d

#### Elasticsearch
  - Create a template for new log type which specifies one shard per index. Also set
  the alias to point to the document_type name specified in filebeat.yml

#### Curator
  - Create curator job to delete indices older than 21 days for app logs


Before you start filebeat on the client machine, open cerebro and then start filebeat
to make sure the indice is created properly and the shards are set to 1 as well as the alias
being set properly.

### Re-indexing

POST _reindex
{
  "source": {
    "index": "twitter"
  },
  "dest": {
    "index": "new_twitter"
  }
}

curl -XPOST 'localhost:9200/_reindex?pretty' -H 'Content-Type: application/json' -d'
{
  "source": {
    "index": "twitter"
  },
  "dest": {
    "index": "new_twitter"
  }
}
'


PUT /twitter/_settings
{
    "index" : {
        "refresh_interval" : "-1"
    }
}
