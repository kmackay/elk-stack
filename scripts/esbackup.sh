#!/bin/bash
es_host="http://kibana:kibana@10.1.2.136:9200"
bkpath="/kmackay/gitted/elk-stack/indextemplates"

for app in xml qs2 streamer ums qt syslogs elkherd entitlement loaders-prod; do /usr/bin/curl -s "$es_host"/"$app"/_settings > "$bkpath"/"$app"-$(date +"%m-%d-%y")-settings.json \
    && /usr/bin/curl -s "$es_host"/"$app"/_mapping > "$bkpath"/"$app"-$(date +"%m-%d-%y")-mappings.json; \
done
