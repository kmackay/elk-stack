#!/bin/bash
# Create new index [x]
# Assign rollover alias to new index [x]
# Assign kibana index alias to new index as well [x]
# Create index template 
# args: newindex

es_host="kibana:kibana@10.1.2.136:9200"

echo "Creating $1 index...."
index="$1-1"


curl -X PUT "$es_host/$index" -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 1,
            "number_of_replicas" : 0
        }
    }
}'

# sleep 5

echo "Assigning aliases: to newly created index..."
curl -X POST "$es_host/_aliases" -H 'Content-Type: application/json' -d '
{
    "actions" : [
        { "add" : { "index" : "'$index'", "alias" :"'$1roll'" } },
        { "add" : { "index" : "'$index'", "alias" :"'$1'" } }
    ]
}?pretty
'

curl -X PUT "$es_host/_template/$1_tpl" -H 'Content-Type: application/json' -d'
{
  "order": 0,
  "index_patterns": [
    "'$1-*'"
  ],
  "settings": {
    "index": {
      "number_of_shards": "1",
      "number_of_replicas": "0"
          }
  },
  "mappings": {},
  "aliases": {
    "'$1'": {}
  }
}
'
