#!/bin/bash
es_host="http://kibana:kibana@10.1.2.136:9200"
/usr/bin/curl -s -o /kmackay/gitted/elk-stack/indextemplates/elkherd_idxtmpl_$(date +"%m-%d-%y").log $"es_host"/_template?pretty
/usr/bin/git add /kmackay/gitted/elk-stack/indextemplates/elkherd_idxtmpl_$(date +"%m-%d-%y").log
/usr/bin/git commit -m "Template backup for $(date +"%m-%d-%y")"
/usr/bin/git push
