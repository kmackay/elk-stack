#!/bin/bash

#################################################################
# Creation: Edouard Lamoine
# Last Modification: 27 mai 2015
# This script is verifying the elasticsearch status
#
# USAGE: ./check_elasticsearch_health HOST_ADDRESS
#
# ex: ./check_elasticsearch_health localhost
# with Shield:
# ./check_elasticsearch localhost monitoring_user secret password
#
#
#################################################################

#Memo for Nagios outputs
#STATE_OK=0
#STATE_WARNING=1
#STATE_CRITICAL=2
#STATE_UNKNOWN=3

type curl >/dev/null 2>&1 || { echo >&2 "This plugin require curl but it's not installed."; exit 3; }

function usage {
echo "Usage: $(basename $0) HOST_ADDRESS USERNAME PASSWORD"
}

HOST=$1
USERNAME=$2
PASSWORD=$3

if [ -z $USERNAME -a -z $PASSWORD ]
then CREDENTIALS=""
else
CREDENTIALS=" -u $USERNAME:$PASSWORD"
fi

STATUS=`/usr/bin/curl $CREDENTIALS -s $HOST:9200/_cluster/health?pretty|grep status|awk '{print $3}'|cut -d\" -f2`

if [[ $STATUS && "$STATUS" != "green" ]]; then
echo "CRITICAL - Status is $STATUS"
exit 2
fi

if [[ "$STATUS" == "green" ]]; then
echo "OK - Status is $STATUS"
exit 0
fi

echo "UNKNOWN - No data were returned by elastisearch on host $HOST"
exit 3
