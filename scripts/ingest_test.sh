#!/bin/bash
set -x
LOGSRC="/home/kamen/clogs/vitara"
# Dont change following line
LOGDST="/home/kamen/tlog/qs2"
sudo service filebeat stop
sudo rm /home/kamen/tmp/registry
rm -rf $LOGDST/*
find $LOGSRC/qt* -mtime 5 -exec rsync -av --progress -v {} $LOGDST \;
cd "$LOGDST"
tar -xvf *.tar
rm *.tar
unpigz *
#rm *.tar
ssh -t elk "curl -XDELETE http://localhost:9200/qs2test*"
date && du -h /home/kamen/tlog/qs2/
sudo /usr/bin/filebeat.sh -c /etc/filebeat/filebeat_qs2.yml -E name=qs2test
